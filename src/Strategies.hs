-- |
-- Module      : Strategies
-- Copyright   : (c) 2008 Bertram Felgenhauer
-- License     : BSD3
--
-- Maintainer  : Bertram Felgenhauer <int-e@gmx.de>
-- Stability   : experimental
-- Portability : portable
--
-- This module is part of Haskell PGMS.
--
-- Import and export all available strategies.
--

module Strategies (
    strategies,

    simpleStrat,
    strat1,
) where

import Mine
import SimpleStrat
import Strat1

strategies :: [Strategy]
strategies = [strat1, simpleStrat]

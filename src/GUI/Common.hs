-- |
-- Module      : GUI.Common
-- Copyright   : (c) 2008 Bertram Felgenhauer
-- License     : BSD3
--
-- Maintainer  : Bertram Felgenhauer <int-e@gmx.de>
-- Stability   : experimental
-- Portability : portable
--
-- This module is part of Haskell PGMS.
--
-- Common definitions for GUI
--

module GUI.Common (
    Globals (..),
    State (..),
    initState,
    maxSize,
) where

import Mine

import qualified Graphics.UI.Gtk as G
import Data.IORef

-- maximum board size supported by GUI
maxSize :: Pos
maxSize = Pos 30 16

-- global GUI variables
data Globals = Globals {
    gBoard     :: G.DrawingArea,
    gStatusbar :: G.Statusbar,
    gState     :: IORef State
}

-- mutable GUI state
data State = State {
    sConfig   :: Config,
    sStrategy :: Strategy,
    sBoard    :: Maybe Board,
    sStop     :: Maybe (IO ()),
    sPreviousConfigItem :: Maybe G.RadioMenuItem
}

-- initial state
initState strats = State {
    sConfig   = beginner,
    sStrategy = head strats,
    sBoard    = Nothing,
    sStop     = Nothing,
    sPreviousConfigItem = Nothing
}

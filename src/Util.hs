-- |
-- Module      : Util
-- Copyright   : (c) 2008 Bertram Felgenhauer
-- License     : BSD3
--
-- Maintainer  : Bertram Felgenhauer <int-e@gmx.de>
-- Stability   : experimental
-- Portability : portable
--
-- This module is part of Haskell PGMS.
--
-- Miscellaneous utility functions
--

module Util (
    findFile,
    formatString
) where

import Paths_hs_pgms

import System.Directory
import Data.Char
import Data.List

-- find a data file
-- this uses Paths_mine.getDataFileName, but also looks
-- in a few other places for convenient in-place running.
findFile :: FilePath -> IO FilePath
findFile name = do
    let scan :: [IO FilePath] -> IO FilePath
        scan [] = error $ "Couldn't find file '" ++ name ++ "'"
        scan (c:cs) = do
            f <- c
            b <- doesFileExist f
            if b then return f else scan cs
    scan [getDataFileName $ "data/" ++ name,
          return $ "data/" ++ name,
          return $ "../data/" ++ name,
          return $ name]

-- format a text string to fit nicely into 78 columns of text.
formatString :: String -> String
formatString = unlines . concatMap (block . (++" ")) . intersperse "" . lines
  where
    block "" = []
    block text = let
        (chunk, rest) = splitAt 78 text
        (end', start') = span (not . isSpace) (reverse chunk)
      in
        if null start' then chunk : block (dropWhile isSpace rest)
                       else reverse (dropWhile isSpace start')
                            : block (reverse end' ++ rest)
